/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_string.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsiquet <lsiquet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 15:15:14 by lsiquet           #+#    #+#             */
/*   Updated: 2018/07/15 18:41:27 by lsiquet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRING_H
# define FT_STRING_H

int		ft_strcmp(char *s1, char *s2);
int		ft_strlen(char *str);
int		ft_strcont(char *str, char c);
int		ft_atoi(char *str);
int		ft_atol(char *str);
int		ft_strcpy(char *str);

char	*ft_strstr(char *str, char *to_find);
char	*ft_strupcase(char *str);
char	*ft_strlowcase(char *str);
char	*ft_strcapitalize(char *str);

int		ft_str_is_alpha(char *str);
int		ft_str_is_numeric(char *str);
int		ft_str_is_printable(char *str);

#endif

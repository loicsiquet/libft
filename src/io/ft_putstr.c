/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsiquet <lsiquet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 11:45:37 by lsiquet           #+#    #+#             */
/*   Updated: 2018/07/15 18:27:17 by lsiquet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../headers/ft_io.h"

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}
